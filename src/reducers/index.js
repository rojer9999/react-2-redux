import { combineReducers } from 'redux';

import chat from '../containers/Chat/reducer';
import modal from '../containers/Modal/reducer';
import mesagesList from '../containers/MesagesList/reducer';

const rootReducer = combineReducers({
  chat,
  modal,
  mesagesList
})

export default rootReducer;