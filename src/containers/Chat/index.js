import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import Spinner from '../../components/Spinner';
import Header from '../../components/Header';
import UsersList from '../../components/UsersList';
import MesagesList from '../MesagesList';
import Modal from '../Modal';
import { getHeaderData } from './service';
import { gotUsers } from './actions';

class Chat extends Component {
  componentDidUpdate(prevProps) {
    const { messages, gotUsers } = this.props;
    if (prevProps.messages.length !== messages.length) {
      gotUsers(messages);
    }
  }
  
  render() {
    const { headerData, isLoading, users } = this.props;

    return (
      <Fragment>
        <Modal />
        <Spinner isLoading={isLoading} />
        <Header headerData={headerData} />
        <div className="container">
          <div className="messaging">
            <div className="inbox_msg">
              <UsersList users={users} />
              <MesagesList />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ chat, mesagesList }) => ({
  isLoading: mesagesList.isLoading,
  headerData: getHeaderData(mesagesList.messages),
  users: chat.users,
  messages: mesagesList.messages
});

const mapDispatchToProps = {
  gotUsers
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);