import moment from 'moment';

const getUsers = (messages) => {
  let flags = [], output = [], l = messages.length, i;
  for (i = 0; i < l; i++) {
    if (flags[messages[i].user] || messages[i].user === 'Me'){
      continue;
    }
    flags[messages[i].user] = true;
    output.push({
      user: messages[i].user,
      avatar: messages[i].avatar
    });
  }
  return [...new Set(output)];
};

const getHeaderData = (messages) => {

  const getLastMessageAt = () => {
    let time = 0;
    messages.forEach(({ created_at }) => {
      const timestamp = new Date(created_at).getTime();
      if (time < timestamp) {
        time = timestamp;
      }
    });
    return moment(time).format("Do MM, HH:MM");
  }

  return {
    totalUsers: getUsers(messages).length,
    totalMessages: messages.length,
    lastMessageAt: getLastMessageAt()
  }
}

export {
  getHeaderData,
  getUsers
}