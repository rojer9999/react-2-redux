import {
  USERS_FETCHED
} from './actionTypes';

import { getUsers } from './service';

const initialState = {
  users: []
};

export default function (state = initialState, action) {
  switch (action.type) {

    case USERS_FETCHED:
      const { users } = action.payload;
      return {
        ...state,
        users: getUsers(users)
      }
      
    default:
      return state;
  }
}