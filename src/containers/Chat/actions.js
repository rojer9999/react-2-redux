import {
  USERS_FETCHED
} from './actionTypes';

export const gotUsers = (users) => ({
  type: USERS_FETCHED,
  payload: {
    users
  }
});
