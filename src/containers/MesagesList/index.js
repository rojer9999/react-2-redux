import React, { Component, Fragment } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';

import Message from '../../components/Message';
import MesageInput from '../../components/MesageInput';
import {
  asyncFetchingMessages,
  putLike,
  onNewMessage,
  onDeleteMessage
} from './actions';
import { showModal } from '../Modal/actions';
import { lastMsgId } from './service';

let divider = '';
let prevDivider = ''

const getDividerText = (message) => {
  const daysDiff = moment.utc(moment().diff(message.created_at)).format('DD') - 1;
  if (prevDivider === daysDiff) {
    return false;
  } else {
    prevDivider = daysDiff;
  }

  if (daysDiff === 1) {
    divider = 'Yesterday';
  } else {
    divider = moment(message.created_at).fromNow();
  }

  return <div>{divider}</div>
}

class MesagesList extends Component {
  componentDidMount() {
    const { asyncFetchingMessages } = this.props;
    asyncFetchingMessages();
  }

  render() {
    const {
      messages,
      onDeleteMessage,
      putLike,
      onNewMessage,
      showModal
    } = this.props;

    const lastMsg = lastMsgId(messages);

    const printMessages = () => {
      return messages.map((message) => {
        return (
          <Fragment key={message.id}>
            {getDividerText(message)}
            <Message
              onDeleteMessage={onDeleteMessage}
              messageObj={message}
              onAddLike={putLike}
              showModal={showModal}
              lastMsg={lastMsg}
            />
          </Fragment>
        )
      })
    }
  
    return (
      <div className="mesgs">
      <div className="msg_history">
        {printMessages()}
      </div>
      <MesageInput onNewMessage={onNewMessage} />
    </div>
    );
  }
};

const mapStateToProps = ({ mesagesList }) => ({
  messages: mesagesList.messages
})

const mapDispatchToprops = {
  asyncFetchingMessages,
  putLike,
  onNewMessage,
  onDeleteMessage,
  showModal
};

export default connect(mapStateToProps, mapDispatchToprops)(MesagesList);