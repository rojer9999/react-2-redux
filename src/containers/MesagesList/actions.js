import {
  MESSAGES_FETCHED,
  MESSAGES_FETCHING,
  MESSAGES_FETCHING_ERROR,
  PUT_LIKE,
  NEW_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE
} from './actionTypes';
import {
  addLikeField,
  onAddLike,
  createNewMessage
} from './service';

export const asyncFetchingMessages = () => (dispatch) => {
  fetch('https://api.myjson.com/bins/1hiqin')
    .then(response => response.json())
    .then(messages => {
      const messagesWithLikes = addLikeField(messages);
      dispatch(fetchMessages(messagesWithLikes));
    })
    .catch(() => dispatch(fetchingMessagesError()))
}

export const fetchMessages = (data) => ({
  type: MESSAGES_FETCHED,
  payload: {
    data
  }
});

export const fetchingMessages = () => ({
  type: MESSAGES_FETCHING,
});

export const fetchingMessagesError = () => ({
  type: MESSAGES_FETCHING_ERROR,
});

export const putLike = (id) => ({
  type: PUT_LIKE,
  payload: {
    likeId: onAddLike(id)
  }
});

export const onNewMessage = (message) => ({
  type: NEW_MESSAGE,
  payload: {
    message: createNewMessage(message)
  }
});

export const onDeleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});

export const onUpdateMessage = (id, message) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    message
  }
});
