import moment from 'moment';
    
const lastMsgId = (messages) => {
  const len = messages.length - 1;
  for (let i = len; i > 0; i--) {
    const { user, id } = messages[i];
    if (user === 'Me') {
      return id;
    }
  }
}

const getUsers = (messages) => {
  let flags = [], output = [], l = messages.length, i;
  for (i = 0; i < l; i++) {
    if (flags[messages[i].user]) continue;
    flags[messages[i].user] = true;
    output.push({
      user: messages[i].user,
      avatar: messages[i].avatar
    });
  }
  return [...new Set(output)];
};

const createNewMessage = (message) => ({
  id: moment.now() + '',
  user: 'Me',
  avatar: '',
  created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
  message: message,
  marked_read: false,
  likes: 0,
});

const addLikeField = (messages) => {
  const storage = JSON.parse(localStorage.getItem('likes'));
  return messages.map(message => {
    message.likes = 0;
    if (storage && storage.includes(message.id)) {
      message.likes += 1;
    }

    return message;
  });
}

const onAddLike = (id) => {
  const storage = JSON.parse(localStorage.getItem('likes'));
  if (storage && storage.includes(id)) {
    return false;
  }

  if (Array.isArray(storage)) {
    storage.push(id);
    localStorage.setItem('likes', JSON.stringify(storage));
  } else {
    localStorage.setItem('likes', JSON.stringify([id]));
  }

  return id;
}

const getHeaderData = (messages) => {

  const getLastMessageAt = () => {
    let time = 0;
    messages.forEach(({ created_at }) => {
      const timestamp = new Date(created_at).getTime();
      if (time < timestamp) {
        time = timestamp;
      }
    });
    return moment(time).format("Do MM, HH:MM");
  }

  return {
    totalUsers: getUsers(messages).length,
    totalMessages: messages.length,
    lastMessageAt: getLastMessageAt()
  }
}

export {
  addLikeField,
  getHeaderData,
  onAddLike,
  createNewMessage,
  getUsers,
  lastMsgId
}