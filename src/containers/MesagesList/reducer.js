import {
  MESSAGES_FETCHED,
  MESSAGES_FETCHING,
  MESSAGES_FETCHING_ERROR,
  PUT_LIKE,
  NEW_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE
} from './actionTypes';

import { getUsers } from './service';

const initialState = {
  messages: [],
  users: [],
  isLoading: true,
  fetchingError: false
};

export default function (state = initialState, action) {
  switch (action.type) {

    case MESSAGES_FETCHED:
      const { data } = action.payload;
      return {
        ...state,
        messages: data,
        users: getUsers(data),
        fetchingError: false,
        isLoading: false
      }

    case MESSAGES_FETCHING:
      return {
        ...state,
        isLoading: true
      }

    case MESSAGES_FETCHING_ERROR:
      return {
        ...state,
        isLoading: false,
        fetchingError: true
      }

    case PUT_LIKE:
      const { likeId } = action.payload;
      const messages = state.messages.map(message => {
        if (message.id === likeId) {
          message.likes += 1;
        }
        return message;
      });
      
      if (!likeId) {
        return state;
      } else {
        return {
          ...state,
          messages
        }
      }

    case NEW_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.payload.message]
      }

    case DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter(message => message.id !== action.payload.id)
      }

    case UPDATE_MESSAGE:
      const { id, message } = action.payload;
      const updatedMessages = state.messages.map(msg => {
        if (msg.id === id) {
          msg.message = message;
          return msg;
        } else {
          return msg;
        }
      });

      return {
        ...state,
        messages: updatedMessages
      }
      
    default:
      return state;
  }
}