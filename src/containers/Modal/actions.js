import { SHOW_MODAL, HIDE_MODAL } from './actionTypes';

export const showModal = (id) => ({
  type: SHOW_MODAL,
  payload: {
    id
  }
});

export const hideModal = () => ({
  type: HIDE_MODAL
});