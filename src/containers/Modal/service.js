const getMessageById = (messages, id) => {
  const msg = messages.find((message) => message.id === id);
  if (msg && msg.message) {
    return msg.message
  } else {
    return ''
  }
}

export {
  getMessageById
}