import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';
import { hideModal } from './actions';
import { onUpdateMessage } from '../MesagesList/actions';
import { getMessageById } from './service';

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputVal: ''
    }

    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { editingMessage, isShown } = this.props;
    if (prevProps.isShown !== isShown) {
      this.setState(() => ({ inputVal: editingMessage }));
    }
  }

  onChange(e) {
    this.setState({ inputVal: e.target.value });
  }

  onSave() {
    const { messageId, onUpdateMessage, hideModal } = this.props;
    const { inputVal } = this.state;
    onUpdateMessage(messageId, inputVal);
    hideModal();
  }

  render(){
    const { isShown, hideModal } = this.props;
    const { inputVal } = this.state;

    const modal = (
      <div id="main-modal" className="overlay">
        <div className="popup">
          <h2>Edit</h2>
          <div className="close" onClick={hideModal}>×</div>
          <div className="content">
            <input onChange={this.onChange} value={inputVal} className="input-modal" />
            <div>
              <button onClick={hideModal}>Cancel</button>
              <button onClick={this.onSave}><i className="fa fa-save"></i></button>
            </div>
          </div>
        </div>
      </div>
    );

    return isShown ? modal : null;
  }
};

const mapDispatchToprops = {
  hideModal,
  onUpdateMessage
};

const mapStateToProps = ({ modal, mesagesList }) => ({
  isShown: modal.isShown,
  editingMessage: getMessageById(mesagesList.messages, modal.messageId),
  messageId: modal.messageId
});

export default connect(mapStateToProps, mapDispatchToprops)(Modal);