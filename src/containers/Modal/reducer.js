import { SHOW_MODAL, HIDE_MODAL } from './actionTypes';

const initialState = {
  isShown: false,
  messageId: ''
}

export default function (state = initialState, action) {
  switch (action.type) {
    
    case SHOW_MODAL: {
      const { id } = action.payload;
      return {
        ...state,
        isShown: true,
        messageId: id
      };
    }

    case HIDE_MODAL: {
      return {
        ...state,
        isShown: false,
        messageId: ''
      };
    }

    default:
      return state;
  }
}