import React from 'react';

const UsersList = ({ users }) => {
  const printUsers = () => users.map(({ user, avatar }) => (
    <div key={avatar} className="chat_list">
      <div className="chat_people">
        <div className="chat_img">
          <img src={avatar} alt={user}/>
        </div>
        <div className="chat_ib">
          <h5>{user}</h5>
        </div>
      </div>
    </div>
  ));

  return (
    <div className="inbox_people">
      <div className="headind_srch">
        <div className="recent_heading">
          <h4>Users</h4>
        </div>
      </div>
      <div className="inbox_chat">
        {printUsers()}
      </div>
    </div>
  );
};

export default UsersList;