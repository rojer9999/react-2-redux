import React from 'react';
import './style.css';
import moment from 'moment';

const Message = ({
  messageObj,
  onDeleteMessage,
  onAddLike,
  showModal,
  lastMsg
}) => {
  const { avatar, created_at, likes, id, message, user } = messageObj;
  const time = moment(created_at).format("Do MM, HH:MM");

  const printIncoming = () => (
    <div className="incoming_msg">
      <div className="incoming_msg_img">
        <img src={avatar} alt={user}/>
      </div>
      <div className="received_msg">
        <div className="received_withd_msg">
          <p>{message}</p>
          <p className="footer_msg">
            {likes}
            <i className="fa fa-thumbs-up" onClick={() => onAddLike(id)}></i>
          </p>
          <span className="time_date">{time}</span></div>
      </div>
    </div>
  );

  const printOutgoingMessage = () => (
    <div className="outgoing_msg">
      <div className="sent_msg">
        <p>
          {message}
          <span className="footer_msg">
            {lastMsg === id && <i className="fa fa-cog" onClick={() => showModal(id)}></i>}
            <i className="fa fa-trash" onClick={() => onDeleteMessage(id)}></i>
            {likes}
            <i className="fa fa-thumbs-up"></i>
          </span>
        </p>
        <span className="time_date">{time}</span> </div>
    </div>
  )

  if (user === 'Me') {
    return printOutgoingMessage();
  } else {
    return printIncoming();
  }
};

export default Message;